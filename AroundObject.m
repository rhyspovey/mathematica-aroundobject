(* ::Package:: *)

(* ::Title:: *)
(*AroundObject*)


(* ::Text:: *)
(*Rhys G. Povey*)
(*www.rhyspovey.com*)


(* ::Text:: *)
(*3 November 2023*)
(*- Added IncludeConstantBasis option to AroundLinearModelFit.*)
(*- BlockDiagonalMatrix is now a native Mathematica function.*)
(**)
(*TODO:*)
(*	- Add name collision avoidance to AroundJoin with eg. name(2).*)


(* ::Section:: *)
(*Front End*)


BeginPackage["AroundObject`"];


AroundObject::usage="AroundObject[names,values,covariance] stores named variable information.";


AroundObject::dims="Incompatible dimensions.";


AroundObject::dupe="Duplicate names.";


AroundObject::sym="Covariance matrix is not symmetric.";


AroundObject::str="Variable names must be strings.";


(* ::Subsection:: *)
(*Functions and maps*)


(* ::Text:: *)
(*Built-in AroundReplace provides some functionality here but only returns lists of Around.*)


AroundExpression::usage="AroundExpression[expr,AroundObject] computes expr containing named variables.";


AroundObjectMapping::usage="AroundObjectMapping[AroundObject,rules] uses rules in the form of new->f[original].";


AroundObjectMapping::rules="Rule keys must be new named variable strings.";


AroundObjectReplace::usage="AroundObjectReplace[AroundObject,rules] uses rules in the form of f[original]->new.";


AroundObjectReplace::rules="Rule values must be new named variable strings.";


AroundConstruct::usage="AroundConstruct[f,x] computes f[x] where x is Around or VectorAround.";


(* ::Subsection:: *)
(*Combining*)


AroundJoin::usage="AroundJoin[\!\(\*SubscriptBox[\(AroundObject\), \(1\)]\),\!\(\*SubscriptBox[\(AroundObject\), \(2\)]\),\[Ellipsis]] joins independent AroundObjects together."


AroundMean::usage="AroundMean[\!\(\*SubscriptBox[\(AroundObject\), \(1\)]\),\!\(\*SubscriptBox[\(AroundObject\), \(2\)]\),\[Ellipsis]] computes the simple mean combination of a set of AroundObjects sharing the same parameters.";


AroundWeightedMean::usage="AroundWeightedMean[\!\(\*SubscriptBox[\(AroundObject\), \(1\)]\),\!\(\*SubscriptBox[\(AroundObject\), \(2\)]\),\[Ellipsis]] computes the weighted mean of a set of AroundObjects sharing the same parameters. Has option Method as Inverse or PseudoInverse."


(* ::Subsection:: *)
(*Fitting*)


NamedNonlinearModelFit::usage="NamedNonlinearModelFit[{{\!\(\*SubscriptBox[\(x\), \(1, 1\)]\),\!\(\*SubscriptBox[\(x\), \(1, 2\)]\),\[Ellipsis],\!\(\*SubscriptBox[\(y\), \(1\)]\)},{\!\(\*SubscriptBox[\(x\), \(2, 1\)]\),\!\(\*SubscriptBox[\(x\), \(2, 2\)]\),\[Ellipsis],\!\(\*SubscriptBox[\(y\), \(2\)]\)},\[Ellipsis]},function] returns fit results in an AroundObject. Parameters should be given as strings within function. Uses option GuessFunction on data to make initial parameters guesses. If the dependent variable is Around, the uncertainties will be used as weights.";


NamedNonlinearModelFit::vars="Function argument number (`1`), not equal to data independent variables (`2`).";


AroundLinearModelFit::usage="AroundLinearModelFit[data,{\!\(\*SubscriptBox[\(function\), \(1\)]\),\!\(\*SubscriptBox[\(function\), \(2\)]\),\[Ellipsis]}] performs a LinearModelFit without needing to explicitly specify variables and applying weights if data independent-variable is Around.";


AroundLinearModelFit::paramnames="`` parameters names given but expected ``.";


(* ::Subsection:: *)
(*Misc*)


AroundRound::usage="AroundRound[Around[a,b]] gives a rounded to decimal significant figures based on b.";


(* ::Section:: *)
(*Back End*)


Begin["Private`"];


(* ::Subsection:: *)
(*Formatting*)


AccuracyLimitedSymmetricMatrixQ[matrix_]:=Module[{accuracy},
accuracy=Floor@Min@Flatten[Map[Accuracy,matrix,{2}]];
SymmetricMatrixQ@If[accuracy<\[Infinity],
	Map[Round[#,10^-accuracy]&,matrix,{2}],
	matrix
]
];


MakeBoxes[AroundObject[names_List,values_List,covariance_List],_]^:=With[{
row=Join[{"{"},Riffle[StyleBox[#,FontColor->Darker@Gray,FontFamily->"Helvetica"]&/@names,","],{"}"}]
},
InterpretationBox[StyleBox[RowBox[{"AroundObject","[",PanelBox[RowBox[row],FrameMargins->0],"]"}],SpanMaxSize->Infinity],AroundObject[names,values,covariance]]
]/;And[
If[AllTrue[names,StringQ],True,Message[Message[AroundObject::str]];False],
If[And[Length[values]==Length[names],Dimensions[covariance]=={Length[names],Length[names]}],True,Message[AroundObject::dims];False],
If[DuplicateFreeQ[names],True,Message[AroundObject::dupe];False],
If[AccuracyLimitedSymmetricMatrixQ[covariance],True,Message[AroundObject::sym];False]
];


(* ::Subsection::Closed:: *)
(*Extract*)


AroundObject[names_List,values_List,covariance_List][\[FormalX]_]:=With[
{index=First[FirstPosition[names,\[FormalX]]]},
Around[values[[index]],Sqrt[covariance[[index,index]]]]
]/;MemberQ[names,\[FormalX]];


AroundObject[names_List,values_List,covariance_List][\[FormalS]_List]:=With[
{indices=Table[First[FirstPosition[names,\[FormalX]]],{\[FormalX],\[FormalS]}]},
VectorAround[values[[indices]],covariance[[indices,indices]]]
]/;SubsetQ[names,\[FormalS]];


(* ::Subsection:: *)
(*Special calls*)


AroundObject[names_List,values_List,covariance_List]["Parameters"]:=names;


AroundObject[names_List,values_List,covariance_List]["Values"]:=values;


AroundObject[names_List,values_List,covariance_List]["CovarianceMatrix"]:=covariance;


AroundObject[names_List,values_List,covariance_List]["CorrelationMatrix"]:=Sqrt[Inverse@DiagonalMatrix[Diagonal[covariance]]] . covariance . Sqrt[Inverse@DiagonalMatrix[Diagonal[covariance]]];


AroundObject[names_List,values_List,covariance_List]["Uncertainty"]:=Sqrt/@Diagonal[covariance];


AroundObject[names_List,values_List,covariance_List]["Estimates"]:=MapThread[Rule,{names,values}];


AroundObject[names_List,values_List,covariance_List]["Rules"]:=MapThread[Rule,{names,values}];


AroundObject[names_List,values_List,covariance_List]["Distribution"]:=MultinormalDistribution[values,covariance];


AroundObject[names_List,values_List,covariance_List]["Association"]:=<|"Parameters"->names,"Values"->values,"CovarianceMatrix"->covariance|>;


AroundObject[names_List,values_List,covariance_List]["Display"]:=Row[{Column[names,Alignment->Center],MatrixForm[values],MatrixForm[covariance]},Spacer[1]];


MatrixForm[AroundObject[names_List,values_List,covariance_List]]^:=Row[{Column[names,Alignment->Center],MatrixForm[values],MatrixForm[covariance]},Spacer[1]];


AroundObject/:Part[AroundObject[names_List,values_List,covariance_List],vars_List]:=Module[{
	positions=(First[FirstPosition[names,#]]&/@vars)
},
AroundObject[names[[positions]],values[[positions]],covariance[[positions,positions]]]
]


(* ::Subsection:: *)
(*Conversions from*)


MultinormalDistribution[AroundObject[names_List,values_List,covariance_List]]^:=MultinormalDistribution[values,covariance];


VectorAround[AroundObject[names_List,values_List,covariance_List]]^:=VectorAround[values,covariance];


(* ::Subsection:: *)
(*Conversions to*)


AroundObject[name_String,Around[value_,uncertainty_]]:=AroundObject[{name},{value},{{uncertainty^2}}];


AroundObject[names_List,VectorAround[values_List,covariance_List]]:=AroundObject[names,values,covariance];


(* ::Subsection:: *)
(*Functions and maps*)


AroundExpression[expr_,AroundObject[names_List,values_List,covariance_List]]:=Module[{
posindex,func,jacobianT,functionvalue,jacobianTvalue,newcovariance
},
posindex=PositionIndex[names];
func=Evaluate[expr/.\[FormalX]_String:>Slot[First@Lookup[posindex,\[FormalX]]]]&;
jacobianT=Thread[(Derivative@@#)[func]&/@IdentityMatrix[Length[names]],Function];
functionvalue=func@@values;
jacobianTvalue=jacobianT@@values;
newcovariance=jacobianTvalue\[Transpose] . covariance . jacobianTvalue;
(* output *)
If[ListQ@expr,
	VectorAround[functionvalue,newcovariance],
	Around[functionvalue,Sqrt[newcovariance]]
]
]/;If[And[Length[values]==Length[names],Dimensions[covariance]=={Length[names],Length[names]}],True,Message[AroundObject::dims];False];


AroundObject/:(expr_/.a_AroundObject):=AroundExpression[expr,a];


AroundObjectMapping[AroundObject[names_List,values_List,covariance_List],ruleinput:__Rule|{__Rule}]:=Module[{rules=Flatten[{ruleinput}],p,jacobianT},
jacobianT=Table[D[Values[rules]/.\[FormalX]_String:>p[\[FormalX]],\[FormalX]],{\[FormalX],p/@names}]/.MapThread[Rule,{p/@names,values}];
AroundObject[
	Keys[rules],
	Values[rules]/.MapThread[Rule,{names,values}],
	jacobianT\[Transpose] . covariance . jacobianT
]
]/;If[And[Length[values]==Length[names],Dimensions[covariance]=={Length[names],Length[names]}],True,Message[AroundObject::dims];False];


AroundObjectReplace[AroundObject[names_List,values_List,covariance_List],ruleinput:__Rule|{__Rule}]:=Module[{rules=Flatten[{ruleinput}],p,jacobianT},
jacobianT=Table[D[Keys[rules]/.\[FormalX]_String:>p[\[FormalX]],\[FormalX]],{\[FormalX],p/@names}]/.MapThread[Rule,{p/@names,values}];
AroundObject[
	Values[rules],
	Keys[rules]/.MapThread[Rule,{names,values}],
	jacobianT\[Transpose] . covariance . jacobianT
]
]/;If[And[Length[values]==Length[names],Dimensions[covariance]=={Length[names],Length[names]}],True,Message[AroundObject::dims];False];


AroundObject/:(a_AroundObject/.rules_):=AroundObjectReplace[a,rules]


AroundConstruct[func_,VectorAround[values_List,covariance_List]]:=Module[{
jacobianT,functionvalue,jacobianTvalue,newcovariance
},
jacobianT=Thread[(Derivative@@#)[func]&/@IdentityMatrix[Length[values]],Function];
functionvalue=func@@values;
jacobianTvalue=jacobianT@@values;
newcovariance=jacobianTvalue\[Transpose] . covariance . jacobianTvalue;
(* output *)
If[ListQ@functionvalue,
	VectorAround[functionvalue,Normal@Symmetrize@newcovariance],
	Around[functionvalue,Sqrt[newcovariance]]
]
];


AroundConstruct[func_,Around[value_,uncertainty_]]:=Module[{
jacobianT,functionvalue,jacobianTvalue,newcovariance
},
jacobianT=Thread[(Derivative@@#)[func]&/@IdentityMatrix[1],Function];
functionvalue=func@value;
jacobianTvalue=jacobianT@value;
newcovariance=uncertainty^2 jacobianTvalue\[Transpose] . jacobianTvalue;
(* output *)
If[ListQ@functionvalue,
	VectorAround[functionvalue,Normal@Symmetrize@newcovariance],
	Around[functionvalue,Sqrt[newcovariance]]
]
];


(* ::Subsubsection:: *)
(*Combining*)


AroundJoin[{\[FormalX]__AroundObject}]:=AroundObject[
Join@@Through[{\[FormalX]}["Parameters"]],
Join@@Through[{\[FormalX]}["Values"]],
Normal@BlockDiagonalMatrix@Through[{\[FormalX]}["CovarianceMatrix"]]
];


AroundJoin[\[FormalX]__AroundObject]:=AroundJoin[{\[FormalX]}];


AroundMean[{\[FormalX]__AroundObject}]:=AroundObject[
{\[FormalX]}[[1]]["Parameters"],
Plus@@Through[{\[FormalX]}["Values"]]/Length[{\[FormalX]}],
Plus@@Through[{\[FormalX]}["CovarianceMatrix"]]/Length[{\[FormalX]}]^2
]/;SameQ@@Through[{\[FormalX]}["Parameters"]];


AroundMean[\[FormalX]__AroundObject]:=AroundMean[{\[FormalX]}];


AroundWeightedMean[{\[FormalX]__AroundObject},opts:OptionsPattern[]]:=AroundObject[
{\[FormalX]}[[1]]["Parameters"],
OptionValue[Method][Plus@@(OptionValue[Method]/@Through[{\[FormalX]}["CovarianceMatrix"]])] . Plus@@MapThread[Dot,{OptionValue[Method]/@Through[{\[FormalX]}["CovarianceMatrix"]],Through[{\[FormalX]}["Values"]]}],
OptionValue[Method][Plus@@(OptionValue[Method]/@Through[{\[FormalX]}["CovarianceMatrix"]])]
]/;SameQ@@Through[{\[FormalX]}["Parameters"]];


AroundWeightedMean[\[FormalX]__AroundObject,opts:OptionsPattern[]]:=AroundWeightedMean[{\[FormalX]},opts];


Options[AroundWeightedMean]={Method->PseudoInverse};


(* ::Subsection:: *)
(*Fitting*)


NamedNonlinearModelFit[data_List/;Depth[data]>2,funcinput_,opts:OptionsPattern[]]:=Module[{
fixedparams=Keys[OptionValue["Fixed"]],
params,nargs,func,cons,
x,p,fitparams,guesses,args,
fitfunc,fitcons,
fit,
dataval,weights,
covar
},

(* input *)
Switch[Head[funcinput],
	Function,func=funcinput;cons={},
	List,func=First@funcinput;cons=Rest@funcinput
];
params=Union@Cases[func,_String,\[Infinity]];
nargs=Length@Union@Switch[Length@func,1,Cases[func,Slot[_],\[Infinity]],2,First@func];

(* check dimensions *)
If[Dimensions[data][[2]]-1=!=nargs,
Message[NamedNonlinearModelFit::vars,nargs,Dimensions[data][[2]]];Return[$Failed]
];

(* fixed parameters *)
Do[p[\[FormalS]]=Lookup[OptionValue["Fixed"],\[FormalS]],{\[FormalS],fixedparams}];
fitparams=Complement[params,fixedparams];

(* guesses *)
guesses=Join[
Association@If[OptionValue["GuessFunction"]=!=None,OptionValue["GuessFunction"][data],{}],
Association@OptionValue["Guesses"]
];
If[OptionValue[Verbose],Print["Guess: ",guesses]];

(* weights *)
If[Union[Head/@data[[All,-1]]]==={Around},
dataval=MapAt[#["Value"]&,data,{All,-1}];weights=(#[[-1]]["Uncertainty"]^-2&/@data);,
dataval=data;weights=(1&);
];

(* fit *)
fitfunc=func/.\[FormalX]_String:>p[\[FormalX]];
fitcons=cons/.\[FormalX]_String:>p[\[FormalX]];
args=Table[x@\[FormalI],{\[FormalI],nargs}];

fit=NonlinearModelFit[
	dataval,
	Evaluate[If[Length[cons]>0,{fitfunc@@args}~Join~fitcons,fitfunc@@args]],
	Evaluate[If[KeyExistsQ[guesses,#],{p[#],Lookup[guesses,#]},p[#]]&/@fitparams],
	args,
	Weights->weights
];
If[OptionValue[Verbose],Print["Fit: ",fit["BestFitParameters"]/.p[\[FormalX]_]:>\[FormalX]]];

(* picture *)
If[OptionValue[Verbose]\[And](nargs==1),Print[Show[{
	ListPlot[data,PlotRange->Full],
	Plot[Evaluate[fitfunc[x[1]]/.fit["BestFitParameters"]],Evaluate[{x[1]}~Join~MinMax[data[[All,1]]]],PlotStyle->Directive[Red,Dashed],PlotRange->Full]
}]]];

(* fix exact fit *)
covar=If[MissingQ[fit["CovarianceMatrix"]]\[And]OptionValue["MissingToZero"],
ConstantArray[0,{Length@fitparams,Length@fitparams}],
fit["CovarianceMatrix"]
];

(* output *)
AroundObject[
	Keys[fit["BestFitParameters"]]/.p[\[FormalX]_]:>\[FormalX],
	Values[fit["BestFitParameters"]],
	covar
]
];


Options[NamedNonlinearModelFit]={"GuessFunction"->None,"Guesses"->{},"Fixed"->{},Verbose->False,"MissingToZero"->False};


AroundLinearModelFit[data_List,funcsinput_,opts:OptionsPattern[]]:=Module[{
paramnames,
arraydepth=ArrayDepth[data],funclist=If[ListQ[funcsinput],funcsinput,{funcsinput}],
nargs,args,x,
dataval,weights,
fit,
domain
},

(* args *)
nargs=Switch[arraydepth,
	1,1,
	2,Dimensions[data][[2]]-1
];
args=Table[x@\[FormalI],{\[FormalI],nargs}];

(* weights *)
Switch[arraydepth,
	1,If[Union[Head/@data]==={Around},
		dataval=Map[#["Value"]&,data];weights=Map[#["Uncertainty"]^-2&,data];,
		dataval=data;weights=(1&);
	],
	2,If[Union[Head/@data[[All,-1]]]==={Around},
		dataval=MapAt[#["Value"]&,data,{All,-1}];weights=Map[#[[-1]]["Uncertainty"]^-2&,data];,
		dataval=data;weights=(1&);
	]
];

(* fit *)
fit=LinearModelFit[
	dataval,
	Through[funclist@@args],
	args,
	Weights->weights,
	IncludeConstantBasis->OptionValue[IncludeConstantBasis]
];
If[OptionValue[Verbose],Print["Fit: ",fit["BestFitParameters"]]];

(* picture *)
domain=Switch[arraydepth,
	1,{1,Length[data]},
	2,MinMax[data[[All,1]]]
];
If[OptionValue[Verbose]\[And](nargs==1),Print[Show[{
	ListPlot[data,PlotRange->Full],
	Plot[fit[x[1]],Evaluate[{x[1]}~Join~domain],PlotStyle->Directive[Red,Dashed],PlotRange->Full]
}]]];

(* output *)
If[OptionValue["Parameters"]===None,
	paramnames={},
	paramnames=Flatten[{OptionValue["Parameters"]}]
];
If[(Length[paramnames]>0)\[And](Length[paramnames]!=Length[funclist]+1),
	Message[AroundLinearModelFit::paramnames,Length[paramnames],Length[funclist]+1]
];
If[(Length[paramnames]>0)\[And](Length[paramnames]==Length[funclist]+1),
	AroundObject[
		paramnames,
		fit["BestFitParameters"],
		fit["CovarianceMatrix"]
	],
	VectorAround[
		fit["BestFitParameters"],
		fit["CovarianceMatrix"]
	]
]

];


Options[AroundLinearModelFit]={Verbose->False,"Parameters"->None,IncludeConstantBasis->True};


(* ::Subsection:: *)
(*Misc*)


AroundRound[\[FormalX]_Around]:=N@Round[\[FormalX]["Value"],10^Floor[Log[10,\[FormalX]["Uncertainty"]]]];


(* ::Section:: *)
(*End*)


End[];


EndPackage[];


(* ::Section:: *)
(*Scrap*)


(* ::Text:: *)
(*Following don't work: *)
(*Association[AroundObject[names_List,values_List,covariance_List]]^:=<|"Parameters"->names,"Values"->values,"CovarianceMatrix"->covariance|>;*)
(*Rule[AroundObject[names_List,values_List,covariance_List]]^:=MapThread[Rule,{names,values}];*)
(*Lookup[\[FormalA]_AroundObject,\[FormalX]_]^:=\[FormalA][\[FormalX]];*)
(*Lookup[AroundObject[names_List,values_List,covariance_List],\[FormalX]_]^:=AroundObject[names,values,covariance][\[FormalX]];*)


(* ::Text:: *)
(*BlockDiagonalMatrix[inputs_List] := Module[{inputdims, matrixdims, dimcheck},*)
(*   matrixdims = Map[Dimensions, inputs, {1}];*)
(*   Flatten[Join[Sequence @@ #, 2] & /@*)
(*     Table[*)
(*      DeleteCases[DeleteCases[{*)
(*         ConstantArray[0, {matrixdims[[\[FormalI], 1]], Plus @@ matrixdims[[;; \[FormalI] - 1, 2]]}],*)
(*         inputs[[\[FormalI]]],*)
(*         ConstantArray[0, {matrixdims[[\[FormalI], 1]], Plus @@ matrixdims[[\[FormalI] + 1 ;;, 2]]}]*)
(*         }, {}, 2], {}], {\[FormalI], Length[inputs]}],*)
(*    1]*)
(*   ];*)
